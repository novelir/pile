/****************************************************************************
**
** Copyright © 2021 Arthur Autin, Novelir
**
** This file is part of the Novelir Operating System.
**
** Novelir is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** Novelir is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with Novelir. If not, see <https://www.gnu.org/licenses/>.
**
****************************************************************************/

#include <QtCore/QCoreApplication>
#include <QLoggingCategory>
#include <QSettings>
#include <QDir>

#include "remove.hpp"
#include "pile/pile.hpp"

int remove(const QString &arg)
{
// Declare the pile database
    QSettings pileDb(QSettings::NativeFormat, QSettings::SystemScope, QCoreApplication::organizationName(), "pile");


// Check that the target is installed
    pileDb.beginGroup("Pile");
    if ( !pileDb.childGroups().contains(arg) ) {
        QMessageLogger().info() << arg << "is not installed";
        return 0; // Returning 0 to prevent exiting the program when removing multiple piles
    }
    pileDb.endGroup();


// Create the pile object
    Pile pile = Pile().pileCreate(arg);
    pile.isImplicitDependency = false;
    if ( pile.name.isEmpty() ) {
        QMessageLogger().critical() << pile.name << "does not exist";
        return 1;
    }


// Check that the pile has not the tag 'forced'
    if ( pile.isForced ) {
        QMessageLogger().critical() << pile.name << "cannot be removed";
        return 1;
    }


// Check that the pile is not required by another one
    {
        QStringList parent = {};
        pileDb.beginGroup("Pile");
        for ( QString installed : pileDb.childGroups() ) {
            pileDb.beginGroup(installed);
            if ( pileDb.value("dependencyList").toStringList().contains(pile.name) ) {
                parent << installed;
            }
            pileDb.endGroup();
        }
        if ( !parent.isEmpty() ) {
            QMessageLogger().critical() << pile.name << "is required by : " << parent.join(", ");
            return 1;
        }
        pileDb.endGroup();
    }


// Remove each program
    pileDb.beginGroup("Program");
    for ( QString program : pile.programList ) {
        QFile("/resource/program/" + program).remove();
        pileDb.remove(program);
    }
    pileDb.endGroup();


// Remove the repository
    QDir("/pile/" + pile.name).removeRecursively();


// Update rootfs
    pile.sharedUpd();


// Refresh the local database
    pileDb.beginGroup("Pile");
    pileDb.remove(pile.name);
    pileDb.endGroup();


    QMessageLogger().info() << pile.name << "has been removed";


// Removed unused dependencies
    for ( QString dep : pile.dependencyTree ) {
        if ( remove(dep) ) {
            continue;
        }
    }


    return 0;
}
