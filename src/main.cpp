/****************************************************************************
**
** Copyright © 2021 Arthur Autin, Novelir
**
** This file is part of the Novelir Operating System.
**
** Novelir is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** Novelir is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with Novelir. If not, see <https://www.gnu.org/licenses/>.
**
****************************************************************************/

#include <QtCore/QCoreApplication>
#include <QCommandLineParser>
#include <QLoggingCategory>

#include "main.hpp"
#include "info.hpp"
#include "install.hpp"
#include "remove.hpp"
#include "update.hpp"

#include <unistd.h>

int main(int argc, char *argv[])
{
    QCoreApplication::setSetuidAllowed(true);
    setuid(geteuid());
    setgid(getegid());

    QCoreApplication a(argc, argv);
    QCoreApplication::setOrganizationName("novelir");
    QCoreApplication::setApplicationName("pile");
    QCoreApplication::setApplicationVersion("0.0.2");


// Command parser declaration
    QCommandLineParser parser;

    parser.setApplicationDescription("A tool to build piles");

    parser.addHelpOption();

    QCommandLineOption infoOpt({"I", "info"}, "Get information about a pile", "pile");
    QCommandLineOption installOpt({"i", "install"}, "Install a pile", "pile");
    QCommandLineOption removeOpt({"r", "remove"}, "Remove a pile", "pile");
    QCommandLineOption updateOpt({"u", "update"}, "Update a pile", "pile");
    parser.addOptions({infoOpt, installOpt, removeOpt, updateOpt});

    parser.process(QCoreApplication::arguments());


// Check command syntax
    if ( parser.optionNames().size() == 0 ) {
        QMessageLogger().critical("No command specified");
        return 1;
    }
    if ( parser.positionalArguments().size() != 0 ) {
        QMessageLogger().critical("No command specified for argument(s) : %s", qUtf8Printable(parser.positionalArguments().join(", ")));
        return 1;
    }


    int exitCode = 1;
    {
    // pile --info
        for ( QString target : parser.values(infoOpt) ) {
            if ( !target.isEmpty() ) {
                exitCode = info(target);
                if ( exitCode ) {
                    return exitCode;
                }
            }
        }

    // pile --install
        for ( QString target : parser.values(installOpt) ) {
            if ( !target.isEmpty() ) {
                exitCode = install(target);
                if ( exitCode ) {
                    return exitCode;
                }
            }
        }

    // pile --remove
        for ( QString target : parser.values(removeOpt) ) {
            if ( !target.isEmpty() ) {
                exitCode = remove(target);
                if ( exitCode ) {
                    return exitCode;
                }
            }
        }

    // pile --update
        for ( QString target : parser.values(updateOpt) ) {
            if ( !target.isEmpty() ) {
                exitCode = update(target);
                if ( exitCode ) {
                    return exitCode;
                }
            }
        }
    }


    return exitCode;
}
