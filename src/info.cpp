/****************************************************************************
**
** Copyright © 2021 Arthur Autin, Novelir
**
** This file is part of the Novelir Operating System.
**
** Novelir is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** Novelir is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with Novelir. If not, see <https://www.gnu.org/licenses/>.
**
****************************************************************************/

#include <QtCore/QCoreApplication>
#include <QLoggingCategory>
#include <QSettings>

#include "info.hpp"
#include "pile/pile.hpp"

int info(const QString &arg)
{
// Declare the pile database
    QSettings pileDb(QSettings::NativeFormat, QSettings::SystemScope, QCoreApplication::organizationName(), "pile");


// Create the pile object
    Pile pile = Pile().pileCreate(arg);
    if ( pile.name.isEmpty() ) {
        return 1;
    }


// Print information
    QList<QStringList> printCouples;
    printCouples << QStringList({"Name", pile.name})
                 << QStringList({"Revision", pile.revision})
                 << QStringList({"Branch", pile.branch})
                 << QStringList({"Dependencies", pile.dependencyList.join(", ")})
                 << QStringList({"Full dependencies", pile.dependencyTree.join(", ")})
                 << QStringList({"Installed", QString::number(pileDb.childGroups().contains(arg))});

    for ( QStringList couple : printCouples ) {
        QMessageLogger().info() << qUtf8Printable(QString("%1 %2").arg(couple[0]).arg(": " + couple[1], 45));
    }

    return 0;
}
