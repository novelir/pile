/****************************************************************************
**
** Copyright © 2021 Arthur Autin, Novelir
**
** This file is part of the Novelir Operating System.
**
** Novelir is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** Novelir is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with Novelir. If not, see <https://www.gnu.org/licenses/>.
**
****************************************************************************/

#include <QtCore/QCoreApplication>
#include <QFile>
#include <QSettings>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>

#include "pile.hpp"
#include "jsonDl/jsonDl.hpp"

Pile Pile::pileCreate(const QString &arg, const bool &remote, QStringList tree)
{
    Pile pile;

    QSettings pileDb(QSettings::NativeFormat, QSettings::SystemScope, QCoreApplication::organizationName(), "pile");
    pileDb.beginGroup("Pile");

    pile.name = arg;

    pile.isImplicitDependency = pileDb.value(this->name + "/isImplicitDependency").toBool();
    pile.isForced = pileDb.value(this->name + "/forced").toBool();

    pile.repoDb = jsonDl("https://gitlab.com/api/v4/groups/piles/projects?simple=true&include_subgroups=true&order_by=path&sort=asc&per_page=1&page=1&search=" + arg).array()[0].toObject();
    pile.branchDb = jsonDl("https://gitlab.com/api/v4/projects/" + QString::number(pile.repoDb["id"].toInt()) + "/repository/branches").array();

    if ( pileDb.childGroups().contains(arg) ) {
        pile.branch = pileDb.value(this->name + "/branch").toString();
    }
    else {
        for ( QJsonValue branch : pile.branchDb ) {
            for ( QString branchName : {"stable", "candidate", "beta", "alpha"} ) {
                if ( branch.toObject()["name"].toString().startsWith(branchName) && !pile.branchList.contains(branchName) ) {
                    pile.branchList << branchName;
                    if ( pile.branch.isEmpty() ) {
                        pile.branch = branchName;
                    }
                }
            }
        }
    }


// Get the json manifest
    {
    // Use the local manifest
        if ( !remote && pileDb.childGroups().contains(arg) ) {
            QFile file("/pile/" + arg + "/manifest.json");
            if ( file.exists() && file.open(QIODevice::ReadOnly | QIODevice::Text) ) {
                QJsonDocument jsonDoc = QJsonDocument::fromJson(file.readAll());
                file.close();

                pile.manifest = jsonDoc.object();
            }
            else {
                return Pile();
            }
        }


    // Use the remote manifest
        else {
            if ( !pile.repoDb.isEmpty() && pile.repoDb["path"].toString() == arg ) {
                pile.manifest = jsonDl("https://gitlab.com/api/v4/projects/" + QString::number(pile.repoDb["id"].toInt()) + "/repository/files/manifest.json/raw?ref=" + pile.branch + '-' + QSysInfo::currentCpuArchitecture()).object();
            }
            else {
                return Pile();
            }
        }
    }

    pile.revision = pile.manifest["revision"].toString();

    pile.hasKernel = pile.manifest["kernel"].toBool();

    pileDb.endGroup();


// Set the dependency tree
    {
        tree << pile.name;
        for ( QString depName : pile.manifest["dependency"].toVariant().toStringList() ) {

        // Avoid circular dependency
            if ( !tree.contains(depName) ) {

            // Create a pile object for the dependency
                Pile dep = pileCreate(depName, remote, QStringList() << tree << depName);

            // Remove wrong piles
                if ( !dep.name.isEmpty() ) {
                    pile.dependency << dep;

                    pile.dependencyList << dep.name;

                    pile.dependencyTree << dep.name << dep.dependencyList;

                    pile.programTree << dep.programTree;
                }
                else {
                    return Pile();
                }
            }
            else {
                return Pile();
            }
        }
        pile.dependencyTree.removeDuplicates();
    }


// Set the program tree
    {
        pile.program = pile.manifest["program"].toArray();
        for ( QJsonValue program : pile.program ) {
            pile.programList << program.toObject()["id"].toString();
            pile.programTree << program.toObject()["id"].toString();
        }
    }

    return pile;
}


Pile Pile::pileCreate(const QString &arg, const bool &remote)
{
    return Pile::pileCreate(arg, remote, {});
}


Pile Pile::pileCreate(const QString &arg)
{
    return Pile::pileCreate(arg, false, {});
}
