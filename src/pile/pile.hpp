/****************************************************************************
**
** Copyright © 2021 Arthur Autin, Novelir
**
** This file is part of the Novelir Operating System.
**
** Novelir is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** Novelir is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with Novelir. If not, see <https://www.gnu.org/licenses/>.
**
****************************************************************************/

#ifndef PILE_HPP
#define PILE_HPP

#include <QJsonArray>
#include <QJsonObject>

class Pile {
    public:
        QString name;
        QJsonObject repoDb;
        QJsonArray branchDb;
        QJsonObject manifest;

        QStringList branchList;
        QString branch;

        QString revision;

        QStringList dependencyList; // The array of dependencies specified in the manifest
        QList<Pile> dependency;     // The full array of dependencies (QList<Pile>)
        QStringList dependencyTree; // The full array of dependencies (QStringList)
        bool isImplicitDependency;

        bool isForced;
        bool hasKernel;

        QStringList programList;    // The array of programs specified in the manifest
        QJsonArray program;         // The full array of programs from the pile and its dependencies (QJsonArray)
        QStringList programTree;    // The full array of programs from the pile and its dependencies (QStringList)

        void dbUpd();
        Pile pileCreate(const QString &arg);
        Pile pileCreate(const QString &arg, const bool &remote);
        int progUpd();
        int repoUpd();
        void sharedUpd();

    private:
        Pile pileCreate(const QString &arg, const bool &remote, QStringList tree);
};

#endif // PILE_HPP
