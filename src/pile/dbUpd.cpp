/****************************************************************************
**
** Copyright © 2021 Arthur Autin, Novelir
**
** This file is part of the Novelir Operating System.
**
** Novelir is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** Novelir is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with Novelir. If not, see <https://www.gnu.org/licenses/>.
**
****************************************************************************/

#include <QtCore/QCoreApplication>
#include <QSettings>

#include "pile.hpp"

void Pile::dbUpd()
{
// Declare the pile database
    QSettings pileDb(QSettings::NativeFormat, QSettings::SystemScope, QCoreApplication::organizationName(), "pile");


// Update the database
    pileDb.beginGroup("Pile");

    pileDb.setValue(this->name + "/branch", this->branch);
    pileDb.setValue(this->name + "/dependencyList", this->dependencyList);
    pileDb.setValue(this->name + "/implicitDependency", this->isImplicitDependency);
    pileDb.setValue(this->name + "/forced", this->isImplicitDependency);
    pileDb.endGroup();


// Be recursive
    for ( Pile dep : this->dependency ) {
        dep.dbUpd();
    }
}
