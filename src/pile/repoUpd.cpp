/****************************************************************************
**
** Copyright © 2021 Arthur Autin, Novelir
**
** This file is part of the Novelir Operating System.
**
** Novelir is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** Novelir is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with Novelir. If not, see <https://www.gnu.org/licenses/>.
**
****************************************************************************/

#include <QtCore/QCoreApplication>
#include <QSettings>
#include <QDir>
#include <QProcess>

#include "pile.hpp"

int Pile::repoUpd()
{
// Declare the pile database
    QSettings pileDb(QSettings::NativeFormat, QSettings::SystemScope, QCoreApplication::organizationName(), "pile");
    pileDb.beginGroup("Pile");


    QProcess *proc = new QProcess();


// This is an install
    if ( !pileDb.childGroups().contains(this->name) ) {
    // Clean and create the pile's specific directory
        QDir("/pile/" + this->name).removeRecursively();
        QDir().mkpath("/pile/" + this->name);

    // Git clone
        proc->start("git", QStringList() << "clone"
                                         << "--single-branch" << "--branch" << "--depth 1" << this->branch + '-' + QSysInfo::currentCpuArchitecture()
                                         << this->repoDb["http_url_to_repo"].toString()
                                         << "/pile/" + this->name);
    }


// This is an update
    else {
        proc->setWorkingDirectory("/pile/" + this->name);

    // Git pull
        proc->start("git", QStringList() << "pull" << "--rebase" << "--force" << "--update-head-ok" << "--update-shallow");
    }


    proc->waitForFinished(-1);



// Exit on error
    if ( proc->exitCode() != 0 ) {
        return 1;
    }


// Be recursive
    for ( Pile dep : this->dependency ) {
        if ( dep.repoUpd() ) {
            return 1;
        }
    }


    pileDb.endGroup();

    return 0;
}
