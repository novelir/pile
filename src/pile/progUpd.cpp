/****************************************************************************
**
** Copyright © 2021 Arthur Autin, Novelir
**
** This file is part of the Novelir Operating System.
**
** Novelir is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** Novelir is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with Novelir. If not, see <https://www.gnu.org/licenses/>.
**
****************************************************************************/

#include <QtCore/QCoreApplication>
#include <QSettings>
#include <QProcess>
#include <QMimeDatabase>
#include <QDir>

#include "pile.hpp"

int Pile::progUpd()
{
// Get the list of the pile's programs
    for ( QJsonValue value : this->program ) {
        QString program = value.toObject()["id"].toString();
        QStringList mime = value.toObject()["mime"].toVariant().toStringList();
        QSettings pileDb(QSettings::NativeFormat, QSettings::SystemScope, QCoreApplication::organizationName(), "pile");

    // Generate an executable for each program
        {
            pileDb.beginGroup("Program");
            QFile executable("/pile/start/resource/mutable/data/program/" + program);
            executable.remove();

        // Create the executable
            if ( !QFile().link("../../pile/start/resource/program/start", executable.fileName() ) ) {
                return 1;
            }


        // Change the permissions of the executable
            executable.setPermissions(QFileDevice::ReadOwner |
                                      QFileDevice::WriteOwner |
                                      QFileDevice::ExeOwner |
                                      QFileDevice::ReadGroup |
                                      QFileDevice::ExeGroup |
                                      QFileDevice::ReadOther |
                                      QFileDevice::ExeOther);

        // Add it to the database
            pileDb.setValue(program + "/pile", this->name);
            pileDb.endGroup();
        }



    // Register the mimetype(s) which the application can open
        pileDb.beginGroup("Mime");
        for ( QString mimeName : mime ) {
            QMimeType mimeType = QMimeDatabase().mimeTypeForName(mimeName);

            if ( !mimeType.name().isEmpty() && mimeType.isValid() ) {
                if ( !pileDb.value(mimeName).toStringList().contains(program) ) {
                    pileDb.setValue(mimeName, pileDb.value(mimeName).toStringList() << program);
                }
            }
        }
        pileDb.endGroup();
    }


// Be recursive
    for ( Pile dep : this->dependency ) {
        if ( dep.progUpd() ) {
            return 1;
        }
    }

    return 0;
}
