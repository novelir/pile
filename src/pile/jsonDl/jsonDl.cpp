/****************************************************************************
**
** Copyright © 2021 Arthur Autin, Novelir
**
** This file is part of the Novelir Operating System.
**
** Novelir is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** Novelir is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with Novelir. If not, see <https://www.gnu.org/licenses/>.
**
****************************************************************************/

#include <QEventLoop>
#include <QJsonArray>
#include <QJsonDocument>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>

#include "jsonDl.hpp"

QJsonDocument jsonDl(const QString &url)
{
// Initialize the remote database
    QEventLoop eventLoop;
    QNetworkAccessManager manager;
    QObject::connect(&manager, SIGNAL(finished(QNetworkReply*)), &eventLoop, SLOT(quit()));

    QNetworkRequest request(url);
    QNetworkReply *reply = manager.get(request);
    eventLoop.exec();

    if (reply->error() != QNetworkReply::NoError) {
        qDebug() << reply->errorString();
        delete reply;
    }


// Parse the manifest
    QString rawJson = (QString)reply->readAll();
    QJsonDocument jsonDocument = QJsonDocument::fromJson(rawJson.toUtf8());


// Return the document
    return jsonDocument;
}
