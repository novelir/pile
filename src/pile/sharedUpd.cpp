/****************************************************************************
**
** Copyright © 2021 Arthur Autin, Novelir
**
** This file is part of the Novelir Operating System.
**
** Novelir is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** Novelir is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with Novelir. If not, see <https://www.gnu.org/licenses/>.
**
****************************************************************************/

#include <QtCore/QCoreApplication>
#include <QSettings>
#include <QDir>
#include <QFile>
#include <QStandardPaths>
#include <QUrl>

#include <sys/mount.h>

#include "pile.hpp"

void Pile::sharedUpd()
{
// Declare the pile database
    QSettings pileDb(QSettings::NativeFormat, QSettings::SystemScope, QCoreApplication::organizationName(), "pile");
    pileDb.beginGroup("Pile");


    for ( QString dir : {"/resource/immutable", "/resource/mutable", "/resource/program"} ) {
    // Perform a lazy umount
        umount2(qUtf8Printable(dir), MNT_DETACH);


    // Clean and create the directory
        QDir(dir).removeRecursively();
        QDir().mkpath(dir);


    // Declare the list of read-only lowerdirs
        QStringList list = QStringList() << pileDb.childGroups()
                                         << this->dependencyTree
                                         << this->name;
        list.removeDuplicates();


    // /resource/immutable, /resource/mutable
        if ( dir == "/resource/immutable" ||
             dir == "/resource/mutable" ) {

            QStringList lowerdir({});
            for ( QString member : list ) {
                if ( QFile("/pile/" + member + dir).exists() ) {
                    lowerdir.append("/pile/" + member + dir);
                }
            }

        // Declare the mount flags
            unsigned long mountflags = MS_NOEXEC | MS_NODEV | MS_NOSUID | MS_NOATIME;
            if ( dir == "/resource/immutable" ) {
                mountflags = mountflags | MS_RDONLY;
            }

        // OverlayFS
            if ( lowerdir.size() > 1 ) {
                QString workdir = QStandardPaths::standardLocations(QStandardPaths::AppDataLocation)[0] + "/work" + dir;
                QDir(workdir).removeRecursively();
                QDir().mkpath(workdir);

                mount(qUtf8Printable(dir),
                      qUtf8Printable(dir),
                      "overlay",
                      mountflags,
                      qUtf8Printable(QStringList({"lowerdir=" + lowerdir.join(':'),
                                                  "workdir=" + workdir}).join(',')));
            }

        // Bind mount
            else {
                mount(qUtf8Printable(lowerdir[0]),
                      qUtf8Printable(dir),
                      NULL,
                      MS_BIND | mountflags,
                      NULL );
            }
        }

    // /resource/program
        else {
            QDir().mkpath("/pile/start/resource/mutable/data/program");
            mount(qUtf8Printable("/pile/start/resource/mutable/data/program"),
                  qUtf8Printable(dir),
                  NULL,
                  MS_BIND | MS_NODEV | MS_NOSUID | MS_NOATIME | MS_RDONLY,
                  NULL );
        }
    }
    pileDb.endGroup();
}
