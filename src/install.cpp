/****************************************************************************
**
** Copyright © 2021 Arthur Autin, Novelir
**
** This file is part of the Novelir Operating System.
**
** Novelir is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** Novelir is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with Novelir. If not, see <https://www.gnu.org/licenses/>.
**
****************************************************************************/

#include <QtCore/QCoreApplication>
#include <QLoggingCategory>
#include <QSettings>
#include <QFile>

#include "install.hpp"
#include "pile/pile.hpp"

int install(const QString &arg)
{
// Declare the pile database
    QSettings pileDb(QSettings::NativeFormat, QSettings::SystemScope, QCoreApplication::organizationName(), "pile");


// Check that the target is not already installed
    pileDb.beginGroup("Pile");
    if ( pileDb.childGroups().contains(arg) ) {
        QMessageLogger().info() << "The pile is already installed";
        return 0; // Returning 0 to prevent exiting the program when installing multiple piles
    }
    pileDb.endGroup();


// Create the pile object
    Pile pile = Pile().pileCreate(arg);
    pile.isImplicitDependency = false;
    if ( pile.name.isEmpty() ) {
        QMessageLogger().critical() << "The pile does not exist";
        return 1;
    }


// Check that programs are not conflicting with each others
    {
        pileDb.beginGroup("Program");
        QStringList pileList({});
        pileList << pile.name << pile.dependencyTree;

        QStringList programList({});
        programList << pile.programTree << pileDb.childGroups();

        for ( QString program : programList ) {
            pileDb.beginGroup(program);
            if ( !pileList.contains(pileDb.value("pile").toString()) && programList.removeAll(program) > 1 ) {
                QMessageLogger().critical() << "Program conflict";
                return 1;
            }
            pileDb.endGroup();
        }

        pileDb.endGroup();
    }


// Clone the repositories
    if ( pile.repoUpd() ) {
        QMessageLogger().critical() << "Cannot clone the repository";
        return 1;
    }


// Update rootfs
    pile.sharedUpd();


// Refresh the local database
    pile.dbUpd();


// Install the new kernel
    if ( pile.hasKernel ) {
        QFile().remove("/runtime/kernel/boot/vmlinuz-linux-fallback");

        QFile oldKernel = QFile("/runtime/kernel/boot/vmlinuz-linux");
        if ( !oldKernel.exists() || oldKernel.copy("/runtime/kernel/boot/vmlinuz-linux-fallback") ) {
            QMessageLogger().warning() << "Cannot save the the old kernel";
        }

        oldKernel.remove();

        QFile newKernel = QFile("/resource/immutable/libraries/kernel/image-" + pile.name);
        if ( !newKernel.exists() || newKernel.copy("/runtime/kernel/boot/vmlinuz-linux") ) {
            QMessageLogger().critical() << "Cannot install the kernel";
            return 1;
        }
    }


// Install programs
    if ( pile.progUpd() ) {
        QMessageLogger().critical() << "Cannot install a program";
        return 1;
    }


    QMessageLogger().info() << pile.name << "has been installed";
    return 0;
}
