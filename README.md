# Pile - Package manager of Novelir OS


Install programs, fonts, styles, libraries, and more in one universal format; piles are git repositories installed in their own directory, confined with their dependencies.

- **Multi-architecture** : Piles are made to support different hardwares, providing the same experience accross screens
- **Container-like** : Programs are sandboxed to enhance security
- **CI/CD-compliant** : Creating and uploading a pile can be done in a simple shell command
- **GNU/Linux compatible** : Most programs can be easily ported

This program IS NOT another Legacy Linux packaging system, the pile program is part of the **Novelir** operating system project.

[**Novelir is still in heavy development.**](https://novelir.org/)


## Dependencies

### Build dependencies

 - qt6-base
 - cmake
 - make
 
## Installation

    $ mkdir build
    $ cd build
    $ cmake -DCMAKE_INSTALL_PREFIX:PATH=<PREFIX> ..
    $ make
    # make install

Default `<PREFIX>` is `/usr/local`


## Usage

Basic commands to install and remove a pile are :

    # pile --info <pile>
    # pile --install <pile>
    # pile --remove <pile>
    # pile --update <pile>

`BE CAREFUL : YOU SHOULD USE A VM/CHROOT TO PRESERVE YOUR SYSTEM`

For other actions, do :

    $ pile --help


## Links

- **Get involved at [Novelir.org](https://novelir.org/)**
- **Lean more on the [Wiki](https://gitlab.com/novelir/website/-/wikis)**
